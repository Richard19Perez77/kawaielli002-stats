package kawaielli2.puzzleimage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * A class used as the child class of the Main Activity, when the notification of a saved image is clicked to show the image the app will restart.
 * Created by Richard on 11/7/2015.
 */
public class ImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
