package kawaielli2.puzzleimage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.EditText;

/**
 * A class to hold the surface of the puzzle and the thread for updating physics
 * and drawing.
 *
 * @author Rick
 */
public class PuzzleSurface extends SurfaceView implements
        SurfaceHolder.Callback {

    public static final int TRANS_VALUE = (255 / 2);
    public static final int STROKE_VALUE = 5;

    public Paint borderPaintA, borderPaintB, transPaint, fullPaint;

    public AdjustablePuzzle puzzle;
    public PuzzleUpdateAndDraw puzzleUpdateAndDraw;

    public MyMediaPlayer myMediaPlayer;

    public CommonVariables common = CommonVariables.getInstance();

    PuzzleSurface ps;

    private static final String TAG = "puzzleLog";
    public String defaultPuzzleSize = "2";

    public PuzzleSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        // set context for access in other classes
        common.context = context;
        common.res = context.getResources();
        ps = this;

        // register our interest in hearing about changes to our surface
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        borderPaintA = new Paint();
        borderPaintA.setStyle(Paint.Style.STROKE);
        borderPaintA.setStrokeWidth(STROKE_VALUE);
        borderPaintA.setColor(Color.LTGRAY);
        borderPaintA.setAlpha(TRANS_VALUE);

        borderPaintB = new Paint();
        borderPaintB.setStyle(Paint.Style.STROKE);
        borderPaintB.setStrokeWidth(STROKE_VALUE);
        borderPaintB.setColor(Color.DKGRAY);
        borderPaintB.setAlpha(TRANS_VALUE);

        transPaint = new Paint();
        transPaint.setAlpha(TRANS_VALUE);
        transPaint.setStyle(Paint.Style.FILL);

        fullPaint = new Paint();
        puzzleUpdateAndDraw = new PuzzleUpdateAndDraw(holder, context);

    }

    /**
     * Prepare dialog for getting a new image, let the user know the deviant art link is updating as new images are loaded.
     */
    public void nextImage() {
        synchronized (puzzleUpdateAndDraw.mSurfaceHolder) {

            AlertDialog.Builder builder;
            AlertDialog alert;

            builder = new AlertDialog.Builder(common.context);

            builder.setTitle(common.res.getString(R.string.save_image));
            builder.setMessage("Solve time = " + puzzle.getSolveTime() + " secs.");
            builder.setPositiveButton("Yup",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new SavePhoto(common.currentPuzzleImagePosition);
                            common.hideButtons();
                            common.isImageLoaded = false;
                            puzzle.getNewImageLoadedScaledDivided();
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });
            builder.setNegativeButton("Nope",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            common.hideButtons();
                            common.isImageLoaded = false;
                            puzzle.getNewImageLoadedScaledDivided();
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });

            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // if the dialog is canceled give the option to start it
                    // again
                    if (dialog != null)
                        dialog.dismiss();
                }
            });

            alert = builder.create();
            alert.show();
        }
    }

    /**
     * Sets the flag for window focus meaning its able to be interacted with.
     *
     * @param hasWindowFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (common.isLogging)
            Log.d(TAG, "onWindowFocusChanged PuzzleSurface hasWindowFocus:" + hasWindowFocus);

        if (!hasWindowFocus) {
            common.isWindowInFocus = false;
        } else {
            common.isWindowInFocus = true;
            puzzleUpdateAndDraw.updateAndDraw();
        }
    }

    /**
     * On surface create this will be called once with the new screen sizes.
     *
     * @param holder
     * @param format
     * @param width
     * @param height
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        if (common.isLogging)
            Log.d(TAG, "surfaceChanged PuzzleSurface " + width + " " + height);

        puzzleUpdateAndDraw.surfaceChanged(width, height);
        if (common.resumePreviousPuzzle) {
            common.resumePreviousPuzzle = false;
            resumePuzzle();
        } else if (common.createNewPuzzle) {
            common.createNewPuzzle = false;
            createPuzzle();
        }
    }

    /**
     * Keep a reference to our surface holder.
     *
     * @param holder
     */
    public void surfaceCreated(SurfaceHolder holder) {
        if (common.isLogging)
            Log.d(TAG, "surfaceCreated PuzzleSurface");

        puzzleUpdateAndDraw = new PuzzleUpdateAndDraw(holder, common.context);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (common.isLogging)
            Log.d(TAG, "surfaceDestroyed PuzzleSurface");
    }

    @Override
    public boolean performClick() {
        if (common.isLogging)
            Log.d(TAG, "performClick PuzzleSurface");

        super.performClick();
        return true;
    }

    /**
     * Perform Click is called on UP press to perform accessibility type
     * actions.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        synchronized (puzzleUpdateAndDraw.getSurfaceHolder()) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                performClick();
            }

/*            if (common.isWindowInFocus && common.isImageLoaded && !common.isScreenAnimating) {
                if (common.isPuzzleSolved) {
                    common.toggleUIOverlay();
                    return false;
                } else {
                    return puzzle.onTouchEvent(event);
                }
            }*/

            if (common.isWindowInFocus && common.isImageLoaded) {
                if (common.isPuzzleSolved) {
                    common.toggleUIOverlay();
                    return false;
                } else {
                    return puzzle.onTouchEvent(event);
                }
            }

            return super.onTouchEvent(event);
        }
    }

    /**
     * Create a new puzzle based on the sides passed to it.
     *
     * @param sides
     */
    public void createNewSizedPuzzle(int sides) {
        if (common.isLogging)
            Log.d(TAG, "createNewSizedPuzzle PuzzleSurface");

        common.isImageLoaded = false;
        puzzle = new AdjustablePuzzle(ps);
        puzzle.initPieces(sides);
        puzzle.getNewImageLoadedScaledDivided();
        common.hideButtons();
    }

    /**
     * Create a default 3 x 3 puzzle, used if shared prefs fail or first app use.
     */
    public void createPuzzle() {
        if (common.isLogging)
            Log.d(TAG, "createPuzzle PuzzleSurface");

        common.isImageLoaded = false;
        puzzle = new AdjustablePuzzle(ps);
        puzzle.initPieces(3);
        puzzle.getNewImageLoadedScaledDivided();
        common.hideButtons();
    }

    /**
     * Load the previous puzzle from shared preferences.
     */
    public void resumePuzzle() {
        if (common.isLogging)
            Log.d(TAG, "resumePuzzle PuzzleSurface");

        common.isImageLoaded = false;
        puzzle = new AdjustablePuzzle(ps);
        int sides = (int) common.dimensions;
        puzzle.initPieces(sides);
        puzzle.getPrevousImageLoadedScaledDivided();
        common.hideButtons();
    }

    /**
     * Load a new window for the internet link provided.
     */
    public void musicActivity() {
        common.hideButtons();
        Intent intent1 = new Intent(Intent.ACTION_VIEW);
        intent1.setData(Uri.parse(common.context
                .getString(R.string.music_link)));
        common.blogLinksTraversed++;
        common.context.startActivity(intent1);
    }

    /**
     * Load a new window for the internet link provided.
     */
    public void blogActivity() {
        Activity act = (Activity) common.context;
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle(common.res.getString(R.string.deviant_title));
        builder.setMessage(common.context.getString(R.string.artist) + common.res.getString(R.string.deviant_message))
                .setPositiveButton(common.res.getString(R.string.continue_desc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        common.hideButtons();
                        Intent intent2 = new Intent(Intent.ACTION_VIEW);
                        intent2.setData(Uri.parse(common.context.getString(R.string.deviant_art_link)));
                        common.blogLinksTraversed++;
                        common.context.startActivity(intent2);
                    }
                })
                .setNegativeButton(common.res.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //close dialog
                    }
                });
        builder.show();
    }

    public void cleanUp() {
        if (puzzle != null)
            puzzle.recylceAll();
    }

    /**
     * Parse the saved slot order from a saved slot String array.
     *
     * @return
     */
    public String getSlotString() {
        String s = "";
        if (common.puzzleSlots != null)
            for (int i = 0; i < common.puzzleSlots.length; i++) {
                if (common.puzzleSlots[i] != null) {
                    if (i == 0) {
                        s = "" + common.puzzleSlots[i].puzzlePiece.pieceNum;
                    } else {
                        s = s + "," + common.puzzleSlots[i].puzzlePiece.pieceNum;
                    }
                }
            }
        return s;
    }

    public AlertDialog dialog;

    /**
     * On create of a new puzzle ask for the new size wanted, range valid 2 - 7 inclusive.
     */
    public void newPuzzle() {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(common.context);

        builder.setTitle("Create New Puzzle");
        builder.setMessage("Enter number of sides 2 - 7");

        final EditText inputH = new EditText(common.context);
        inputH.setInputType(InputType.TYPE_CLASS_NUMBER);
        inputH.setText(defaultPuzzleSize);
        builder.setView(inputH);

        builder.setPositiveButton("Create",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            String s = inputH.getText().toString();
                            s.replaceAll("[^0-9]", "");
                            int sides = Integer.parseInt(s);

                            if (sides > 7 || sides < 2) {
                                common.showToast("2 to 7 dimension limit");
                            } else {
                                createNewSizedPuzzle(sides);
                            }
                        } catch (NumberFormatException nfe) {
                            common.showToast("Unable to parse number entered.");
                        }
                    }
                });

        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });
        dialog = builder.show();
    }

    /**
     * Set flag for music on/off.
     */
    public void toggleMusic() {
        if (common.playMusic) {
            common.playMusic = false;
            myMediaPlayer.pause();
            common.showToast("Music Off");
        } else {
            common.playMusic = true;
            myMediaPlayer.resume();
            common.showToast("Music On");
        }
    }

    /**
     * Set flag for drawing the border on/off.
     */
    public void toggleBorder() {
        if (common.drawBorders) {
            common.drawBorders = false;
            common.showToast("Borders Off");
        } else {
            common.drawBorders = true;
            common.showToast("Borders On");
        }
    }

    /**
     * Set flag for win chime on/off.
     */
    public void toggleWinSound() {
        if (common.playChimeSound) {
            common.playChimeSound = false;
            common.showToast("Win Effect Off");
        } else {
            common.playChimeSound = true;
            common.showToast("Win Effect On");
        }
    }

    /**
     * Pause the puzzle and draw class.
     */
    public void onPause() {
        if (common.isLogging)
            Log.d(TAG, "onPause PuzzleSurface");

        if (puzzleUpdateAndDraw != null) {
            puzzleUpdateAndDraw.pause();
        }
    }

    /**
     * Set flag for toggle sound on/off.
     */
    public void toggleSetSound() {
        if (common.playTapSound) {
            common.playTapSound = false;
            common.showToast("Set Effect Off");
        } else {
            common.playTapSound = true;
            common.showToast("Set Effect On");
        }
    }

    /**
     * A class to hold the SurfaceHolder and perform the draw operations.
     */
    public class PuzzleUpdateAndDraw {

        public final SurfaceHolder mSurfaceHolder;

        public PuzzleUpdateAndDraw(SurfaceHolder surfaceHolder,
                                   Context context) {
            mSurfaceHolder = surfaceHolder;
            common.context = context;
        }

        /**
         * Lock the canvas before drawing then unlock to perform the draw.
         */
        public void updateAndDraw() {
            if (common.isLogging)
                Log.d(TAG, "updateAndDraw PuzzleSurface");

            Canvas c = null;
            try {
                c = mSurfaceHolder.lockCanvas(null);
                if (c != null) {
                    synchronized (mSurfaceHolder) {
                        updatePhysics();
                        doDraw(c);
                    }
                }
            } finally {
                if (c != null) {
                    mSurfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }

        public SurfaceHolder getSurfaceHolder() {
            return mSurfaceHolder;
        }

        /**
         * The UI is updated on solve of a puzzle where the buttons are shown.
         */
        private void updatePhysics() {
            if (common.isLogging)
                Log.d(TAG, "updatePhysics PuzzleSurface");

            if (common.isPuzzleSolved) {
                common.showButtons();
            }
        }

        /**
         * Draw may include a shadow piece to replace a moving piece.
         *
         * @param canvas
         */
        private void doDraw(Canvas canvas) {
            if (common.isLogging)
                Log.d(TAG, "doDraw PuzzleSurface");

            if (canvas != null) {
                canvas.drawColor(Color.BLACK);
                if (common.isImageLoaded) {
                    if (common.movingPiece) {
                        drawImageWithMovingPiece(canvas);
                    } else {
                        drawImage(canvas);
                    }
                } else {
                    // the imageID is still loading or in error
                    if (common.isImageError) {
                        canvas.drawColor(Color.RED);
                    } else {
                        canvas.drawColor(Color.BLUE);
                    }
                }
            }
        }

        /**
         * Draw each piece at the slot it currently is placed into.
         *
         * @param canvas
         */
        private void drawImage(Canvas canvas) {
            if (common.isLogging)
                Log.d(TAG, "drawImage PuzzleSurface");

            for (int i = 0; i < common.numberOfPieces; i++) {
                if (!common.puzzleSlots[i].puzzlePiece.bitmap
                        .isRecycled()) {
                    // draw pieces
                    canvas.drawBitmap(
                            common.puzzleSlots[i].puzzlePiece.bitmap,
                            common.puzzleSlots[i].puzzlePiece.px,
                            common.puzzleSlots[i].puzzlePiece.py, null);
                    // draw borders
                    if (!common.isPuzzleSolved && common.drawBorders) {
                        canvas.drawRect(
                                common.puzzleSlots[i].sx,
                                common.puzzleSlots[i].sy,
                                common.puzzleSlots[i].sx
                                        + common.puzzleSlots[i].puzzlePiece.bitmap
                                        .getWidth(),
                                common.puzzleSlots[i].sy
                                        + common.puzzleSlots[i].puzzlePiece.bitmap
                                        .getHeight(),
                                borderPaintA);
                    }
                }
            }
        }

        /**
         * Draw the moving piece in full color and the shadow piece in its place as semi-transparent.
         *
         * @param canvas
         */
        private void drawImageWithMovingPiece(Canvas canvas) {
            for (int i = 0; i < common.numberOfPieces; i++) {
                // draw pieces
                if (!common.puzzleSlots[i].puzzlePiece.bitmap
                        .isRecycled()
                        && common.currSlotOnTouchDown != i)
                    canvas.drawBitmap(
                            common.puzzleSlots[i].puzzlePiece.bitmap,
                            common.puzzleSlots[i].puzzlePiece.px,
                            common.puzzleSlots[i].puzzlePiece.py, null);
                // draw border to pieces
                if (!common.isPuzzleSolved && common.drawBorders)
                    canvas.drawRect(
                            common.puzzleSlots[i].sx,
                            common.puzzleSlots[i].sy,
                            common.puzzleSlots[i].sx
                                    + common.puzzleSlots[i].puzzlePiece.bitmap
                                    .getWidth(),
                            common.puzzleSlots[i].sy
                                    + common.puzzleSlots[i].puzzlePiece.bitmap
                                    .getHeight(),
                            borderPaintA);
            }

            // draw moving piece and its shadow
            if (!common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                    .isRecycled()) {

                // draw moving imageID in original location
                canvas.drawBitmap(
                        common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap,
                        common.puzzleSlots[common.currSlotOnTouchDown].sx,
                        common.puzzleSlots[common.currSlotOnTouchDown].sy,
                        transPaint);

                // draw border around original piece location
                canvas.drawRect(
                        common.puzzleSlots[common.currSlotOnTouchDown].sx,
                        common.puzzleSlots[common.currSlotOnTouchDown].sy,
                        common.puzzleSlots[common.currSlotOnTouchDown].sx
                                + common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                                .getWidth(),
                        common.puzzleSlots[common.currSlotOnTouchDown].sy
                                + common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                                .getHeight(), borderPaintB);

                // draw moving piece
                canvas.drawBitmap(
                        common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap,
                        common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.px,
                        common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.py,
                        fullPaint);

                // draw border around moving piece
                if (common.drawBorders)
                    canvas.drawRect(
                            common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.px
                                    + (STROKE_VALUE / 2),
                            common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.py
                                    + (STROKE_VALUE / 2),
                            common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.px
                                    + common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                                    .getWidth()
                                    - (STROKE_VALUE / 2),
                            common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.py
                                    + common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                                    .getHeight()
                                    - (STROKE_VALUE / 2),
                            borderPaintA);
            }
        }

        public void surfaceChanged(int width, int height) {
            // synchronized to make sure these all change atomically
            synchronized (mSurfaceHolder) {
                common.screenW = width;
                common.screenH = height;
            }
        }

        public void pause() {
            synchronized (mSurfaceHolder) {
                if (puzzle != null)
                    puzzle.pause();
            }
        }
    }


}