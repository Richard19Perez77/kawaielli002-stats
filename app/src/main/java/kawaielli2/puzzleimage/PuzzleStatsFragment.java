package kawaielli2.puzzleimage;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A fragment to show the list of stats in text views to the user.
 * Use the {@link PuzzleStatsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PuzzleStatsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    CommonVariables commonVariables = CommonVariables.getInstance();
    TextView puzzlesSolvedTextView;
    TextView twoXtwoPuzzleSolvedCountTextView;
    TextView threeXthreePuzzleSolvedCountTextView;
    TextView fourXfourPuzzleSolvedCountTextView;
    TextView fiveXfivePuzzleSolvedCountTextView;
    TextView sixXsixPuzzleSolvedCountTextView;
    TextView sevenXsevenPuzzleSolvedCountTextView;

    TextView fourRecordSolveTimeTextView;
    TextView nineRecordSolveTimeTextView;
    TextView sixteenRecordSolveTimeTextView;
    TextView twentyfiveRecordSolveTimeTextView;
    TextView thirtysixRecordSolveTimeTextView;
    TextView fourtysevenRecordSolveTimeTextView;

    TextView puzzlesSavedTextView;
    TextView blogLinksTraversedTextView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PuzzleStatsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PuzzleStatsFragment newInstance(String param1, String param2) {
        PuzzleStatsFragment fragment = new PuzzleStatsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //allow menu options to be added
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        updatePuzzleStats();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_stats, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_puzzle_stats, container, false);

        puzzlesSolvedTextView = (TextView) v.findViewById(R.id.puzzlesSolvedTextView);

        twoXtwoPuzzleSolvedCountTextView = (TextView) v.findViewById(R.id.twoXtwoPuzzlesSolvedTextView);
        fourRecordSolveTimeTextView = (TextView) v.findViewById(R.id.twoXtwoSolveTimeTextView);

        threeXthreePuzzleSolvedCountTextView = (TextView) v.findViewById(R.id.threeXthreePuzzlesSolvedTextView);
        nineRecordSolveTimeTextView = (TextView) v.findViewById(R.id.threeXthreeSolveTimeTextView);

        fourXfourPuzzleSolvedCountTextView = (TextView) v.findViewById(R.id.fourXfourPuzzlesSolvedTextView);
        sixteenRecordSolveTimeTextView = (TextView) v.findViewById(R.id.fourXfourSolveTimeTextView);

        fiveXfivePuzzleSolvedCountTextView = (TextView) v.findViewById(R.id.fiveXfivePuzzlesSolvedTextView);
        twentyfiveRecordSolveTimeTextView = (TextView) v.findViewById(R.id.fiveXfiveSolveTimeTextView);

        sixXsixPuzzleSolvedCountTextView = (TextView) v.findViewById(R.id.sixXsixPuzzlesSolvedTextView);
        thirtysixRecordSolveTimeTextView = (TextView) v.findViewById(R.id.sixXsixSolveTimeTextView);

        sevenXsevenPuzzleSolvedCountTextView = (TextView) v.findViewById(R.id.sevenXsevenPuzzlesSovedTextView);
        fourtysevenRecordSolveTimeTextView = (TextView) v.findViewById(R.id.sevenXsevenSolveTimeTextView);

        puzzlesSavedTextView = (TextView) v.findViewById(R.id.imageSavedTextView);

        blogLinksTraversedTextView = (TextView) v.findViewById(R.id.blogLinksTraversedTextView);

        return v;
    }

    /**
     * Runtime method to update view objects, run on UI thread.
     */
    public void updatePuzzleStats() {
        Activity act = getActivity();
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String temp = "" + commonVariables.puzzlesSolved;
                if (puzzlesSolvedTextView != null)
                    puzzlesSolvedTextView.setText(temp);

                temp = "" + commonVariables.fourPiecePuzzleSolvedCount;
                if (twoXtwoPuzzleSolvedCountTextView != null)
                    twoXtwoPuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.fourRecordSolveTime / 1000.0 + " sec.";
                if (fourRecordSolveTimeTextView != null)
                    fourRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.ninePiecePuzzleSolvedCount;
                if (threeXthreePuzzleSolvedCountTextView != null)
                    threeXthreePuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.nineRecordSolveTime / 1000.0 + " sec.";
                if (nineRecordSolveTimeTextView != null)
                    nineRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.sixteenPiecePuzzleSolvedCount;
                if (fourXfourPuzzleSolvedCountTextView != null)
                    fourXfourPuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.sixteenRecordSolveTime / 1000.0 + " sec.";
                if (sixteenRecordSolveTimeTextView != null)
                    sixteenRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.twentyfivePiecePuzzleSolvedCount;
                if (fiveXfivePuzzleSolvedCountTextView != null)
                    fiveXfivePuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.twentyfiveRecordSolveTime / 1000.0 + " sec.";
                if (twentyfiveRecordSolveTimeTextView != null)
                    twentyfiveRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.thirtysixPiecePuzzleSolvedCount;
                if (sixXsixPuzzleSolvedCountTextView != null)
                    sixXsixPuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.thirtysixRecordsSolveTime / 1000.0 + " sec.";
                if (thirtysixRecordSolveTimeTextView != null)
                    thirtysixRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.fourtyninePiecePuzzleSolvedCount;
                if (sevenXsevenPuzzleSolvedCountTextView != null)
                    sevenXsevenPuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.fourtynineRecordsSolveTime / 1000.0 + " sec.";
                if (fourtysevenRecordSolveTimeTextView != null)
                    fourtysevenRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.imagesSaved;
                if (puzzlesSavedTextView != null) {
                    puzzlesSavedTextView.setText(temp);
                }

                temp = "" + commonVariables.blogLinksTraversed;
                if (blogLinksTraversedTextView != null) {
                    blogLinksTraversedTextView.setText(temp);
                }
            }
        });
    }
}
