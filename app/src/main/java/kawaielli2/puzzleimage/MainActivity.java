package kawaielli2.puzzleimage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * The main activity of the application, sets the fragment placeholder to the container view. On resume it will start and animation for the user. Test new images by changeing value in AdjustablePuzzle Class line 99.
 */
public class MainActivity extends AppCompatActivity implements PuzzleFragment.OnPuzzleSolvedListener {

    /**
     * Placeholder is for the fragments to live in.
     */
    public PlaceholderFragment placeHolderFragment;

    /**
     * Start the create method by checking for the current google play services library. Attach the placeholder fragment if not already saved in previous state.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        placeHolderFragment = new PlaceholderFragment();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, placeHolderFragment).commit();
        }
    }

    /**
     * Start logging for Facebook user's. Begin the screen animation for starting the puzzle game.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Stop facebook tracking events.
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Create the default main menu layout.
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * The main menu option is to show the stats fragment if not already, depending on layout, this might not be an option.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // use a switch to run the handler for each item selected
        switch (item.getItemId()) {
            case R.id.menu_stats:
                switchToStatsFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Fragment switching is setting the stats fragment on top of the puzzle, this way when the user backs out of the stats the puzzle is still playable.
     */
    private void switchToStatsFragment() {
        // Add the fragment to the 'fragment_container' FrameLayout
        boolean inStats = false;
        List<android.support.v4.app.Fragment> fragments = getSupportFragmentManager().getFragments();
        for (android.support.v4.app.Fragment f : fragments) {
            if (f instanceof PuzzleStatsFragment) {
                inStats = true;
            }
        }

        if (!inStats) {
            //start the stats fragment
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, PuzzleStatsFragment.newInstance("", "")).addToBackStack("puzzle").commit();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Our listener for the puzzles being solved. A this time the stats should be updated if showing and the listener has fired.
     */
    @Override
    public void setPuzzlesSolved() {
        PuzzleStatsFragment fragment = (PuzzleStatsFragment) placeHolderFragment.getChildFragmentManager().findFragmentById(R.id.puzzleStatsFragment);

        if (fragment != null) {
            // Call a method in the ArticleFragment to update its content
            fragment.updatePuzzleStats();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends
            android.support.v4.app.Fragment {

        public PlaceholderFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main, container,
                    false);
        }
    }
}