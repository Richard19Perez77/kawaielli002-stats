package kawaielli2.puzzleimage;

/**
 * A class to hold the static data used across the application.
 *
 * @author Rick
 */
public class Data {

    public static int TRACK_01 = R.raw.urb;

    public final static int[] PICS = { R.drawable.image1, R.drawable.image2,
            R.drawable.image3, R.drawable.image4, R.drawable.image5,
            R.drawable.image6, R.drawable.image7, R.drawable.image8,
            R.drawable.image9, R.drawable.image10, R.drawable.image11,
            R.drawable.image12, R.drawable.image13, R.drawable.image14,
            R.drawable.image15, R.drawable.image16, R.drawable.image17,
            R.drawable.image18, R.drawable.image19, R.drawable.image20,
            R.drawable.image21, R.drawable.image22, R.drawable.image23,
            R.drawable.image24, R.drawable.image25, R.drawable.image26,
            R.drawable.image27, R.drawable.image28, R.drawable.image29,
            R.drawable.image30, R.drawable.image31, R.drawable.image32,
            R.drawable.image33, R.drawable.image34, R.drawable.image35,
            R.drawable.image36, R.drawable.image37, R.drawable.image38,
            R.drawable.image39, R.drawable.image40, R.drawable.image41,
            R.drawable.image42, R.drawable.image43, R.drawable.image44,
            R.drawable.image45, R.drawable.image46, R.drawable.image47,
            R.drawable.image48, R.drawable.image49, R.drawable.image50,
            R.drawable.image51, R.drawable.image52
    };

}